package com.wavelabs.model;

import org.springframework.stereotype.Component;

/**
 * This class model for Ids
 * 
 * @author tharunkumarb
 *
 */

@Component
public class Ids {
	private int receiverId;
	private int timeslotId;

	public Ids() {

	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public int getTimeslotId() {
		return timeslotId;
	}

	public void setTimeslotId(int timeslotId) {
		this.timeslotId = timeslotId;
	}

}
