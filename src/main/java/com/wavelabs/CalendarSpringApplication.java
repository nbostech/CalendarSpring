package com.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.wavelabs.service","com.wavelabs.resources","com.wavelabs.dao"})
public class CalendarSpringApplication {
	public CalendarSpringApplication() {

	}
	public static void main(String[] args) {
		SpringApplication.run(CalendarSpringApplication.class, args);
	}
}
