package com.wavelabs.service.test;

import com.wavelabs.model.Bookings;
import com.wavelabs.model.Provider;
import com.wavelabs.model.Receiver;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;

public class DataBuilder {
	public static Bookings getBookings() {
		TimeSlots timeslot = new TimeSlots();
		timeslot.setId(1);
		Receiver receiver = new Receiver();
		receiver.setId(1);
		Bookings bookings = new Bookings(1, timeslot, receiver, Status.available);
		return bookings;
	}

	public static TimeSlots getTimeslot() {
		TimeSlots timeslot = new TimeSlots();
		timeslot.setId(1);
		return timeslot;
	}

	public static Receiver getReceiver() {
		Receiver receiver = new Receiver();
		receiver.setId(100);
		return receiver;
	}

	public static Provider getProvider() {
		Provider provider = new Provider();
		provider.setId(100);
		return provider;
	}
	
	public static SlotInfo getSlotInfo(){
		SlotInfo slotinfo=new SlotInfo();
		slotinfo.setId(100);
		return slotinfo;
	}
}
